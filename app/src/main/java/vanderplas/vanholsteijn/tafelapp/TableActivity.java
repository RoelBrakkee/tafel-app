package vanderplas.vanholsteijn.tafelapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class TableActivity extends AppCompatActivity implements View.OnClickListener {

    private int startTime;
    private int difference;
    private int tableValue = 0;
    private int multiplierValue;
    private int[] multiplierValuesList;
    private int[] allPossibleValuesList;
    private TextView textViewAnswerOne;
    private TextView textViewAnswerTwo;
    private TextView textViewAnswerThree;
    private TextView textViewAnswerFour;

    private int goodAnswers = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);

        Bundle b = getIntent().getExtras();
        if(b != null)
            tableValue = b.getInt("table");

        startTime = (int) System.currentTimeMillis();

        setTextViews();
        setOnClickListeners();

        createMultiplierValuesList();

        createQuestionWithAnswers();
    }

    private void setTextViews() {
        textViewAnswerOne = (TextView) findViewById(R.id.textViewAnswerOne);
        textViewAnswerTwo = (TextView) findViewById(R.id.textViewAnswerTwo);
        textViewAnswerThree = (TextView) findViewById(R.id.textViewAnswerThree);
        textViewAnswerFour = (TextView) findViewById(R.id.textViewAnswerFour);
    }

    private void setOnClickListeners() {
        textViewAnswerOne.setOnClickListener(this);
        textViewAnswerTwo.setOnClickListener(this);
        textViewAnswerThree.setOnClickListener(this);
        textViewAnswerFour.setOnClickListener(this);
    }

    private void createMultiplierValuesList() {
        multiplierValuesList = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    }

    private void createAllPossibleValuesList() {
        allPossibleValuesList = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    }

    private void createQuestionWithAnswers() {
        TextView textViewTableValue = (TextView) findViewById(R.id.textViewTableValue);
        setValueInTextView(textViewTableValue, tableValue);

        multiplierValue = getRandomValueFromList(multiplierValuesList);
        multiplierValuesList = deleteValueFromValueList(multiplierValuesList, multiplierValue);

        TextView textViewTableMultiplierValue = (TextView) findViewById(R.id.textViewTableMultiplierValue);
        setValueInTextView(textViewTableMultiplierValue, multiplierValue);

        createAnswers();
    }

    private void setValueInTextView(TextView textView , int value) {
        textView.setText(Integer.toString(value));
    }

    private void createAnswers() {
        createAllPossibleValuesList();

        Integer[] answerValuesList = new Integer[4];
        answerValuesList[0] = multiplierValue;
        answerValuesList[1] = (multiplierValue - 1);
        answerValuesList[2] = (multiplierValue + 1);
        if (multiplierValue == 1) {
            answerValuesList[1] = (multiplierValue + 2);
        }
        else if (multiplierValue == 10) {
            answerValuesList[2] = (multiplierValue - 2);
        }

        for (int i = 0; i <= 2; i++) {
            allPossibleValuesList = deleteValueFromValueList(allPossibleValuesList, answerValuesList[i]);
        }

        int answerMultiplierValue = getRandomValueFromList(allPossibleValuesList);
        answerValuesList[3] = answerMultiplierValue;

        List<Integer> shuffledAnswerValuesList = shuffleValuesList(answerValuesList);

        setAnswersToTextViews(shuffledAnswerValuesList);
    }

    private int getRandomValueFromList(int[] valueList) {
        int rnd = new Random().nextInt(valueList.length);
        return valueList[rnd];
    }

    private List<Integer> shuffleValuesList(Integer[] valuesArray) {
        List<Integer> valuesList  = Arrays.asList(valuesArray);

        Collections.shuffle(valuesList);

        return valuesList;
    }

    private void setAnswersToTextViews(List<Integer> valueList) {
        setValueInTextView(textViewAnswerOne, valueList.get(0) * tableValue);
        setValueInTextView(textViewAnswerTwo, valueList.get(1) * tableValue);
        setValueInTextView(textViewAnswerThree, valueList.get(2) * tableValue);
        setValueInTextView(textViewAnswerFour, valueList.get(3) * tableValue);
    }

    private int[] deleteValueFromValueList(int[] valuesList, int value) {

        int listLength = valuesList.length;

        int[] newValuesList = new int[listLength - 1];

        int j = 0;
        for (int i = 0; i < listLength; i++) {
            if (valuesList[i] != value) {
                newValuesList[j] = valuesList[i];
                j++;
            }
        }

        return newValuesList;
    }

    private void checkGivenAnswer(TextView view) {
        if (Integer.parseInt(view.getText().toString()) == tableValue * multiplierValue) {
            goodAnswers += 1;
        }
    }
    
    private void nextQuestion(){
        createQuestionWithAnswers();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(this);
        alertDialog2.setTitle("Stoppen?");
        alertDialog2.setMessage("Weet je dit zeker?");
        // Add the buttons
        alertDialog2.setPositiveButton("JA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        alertDialog2.show();
    }

    @Override
    public void onClick(View view) {
        checkGivenAnswer((TextView)view);

        if (multiplierValuesList.length >= 1) {
            nextQuestion();
        }
        else {
            difference = ((int) System.currentTimeMillis()) - startTime;

            Intent intent = new Intent(TableActivity.this, ResultActivity.class);

            Bundle bundle = new Bundle();
            bundle.putInt("correctAnswers", goodAnswers); //Your id
            bundle.putInt("multiplicationTable", tableValue);
            bundle.putInt("time", difference / 1000);
            intent.putExtras(bundle); //Put your id to your next Intent
            startActivity(intent);

            finish();
        }
    }
}
