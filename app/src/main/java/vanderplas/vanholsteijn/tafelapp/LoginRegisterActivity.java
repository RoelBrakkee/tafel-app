package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginRegisterActivity extends AppCompatActivity {
    private Button buttonStartLogin;
    private Button buttonStartRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        buttonStartLogin = (Button) findViewById(R.id.buttonStartLogin);
        buttonStartRegister = (Button) findViewById(R.id.buttonStartRegister);

        buttonStartLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoginActivity();
            }
        });

        buttonStartRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegisterActivity();
            }
        });
    }

    private void startLoginActivity(){
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void startRegisterActivity(){
        startActivity(new Intent(this, RegisterActivity.class));
    }


}
