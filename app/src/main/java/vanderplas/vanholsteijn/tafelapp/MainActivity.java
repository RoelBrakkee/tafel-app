package vanderplas.vanholsteijn.tafelapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    private FirebaseUser user;
    private TextView textViewStartPracticing;
    private TextView textViewShowScores;
    private TextView textViewLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        checkLogin();

        textViewStartPracticing = (TextView) findViewById(R.id.textViewStartPracticing);

        textViewStartPracticing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTablesActivityOnClick();
            }
        });

        textViewShowScores = (TextView) findViewById(R.id.textViewGoToScores);

        textViewShowScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ScoresOverviewActivity.class));
            }
        });

        textViewLogout = (TextView) findViewById(R.id.textViewLogout);

        textViewLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                checkLogin();
            }
        });
    }

    public void goToTablesActivityOnClick() {
        Intent k = new Intent(MainActivity.this, TablesListActivity.class);
        startActivity(k);
    }

    private void checkLogin(){
        try{
            user = FirebaseAuth.getInstance().getCurrentUser();
            Log.d("user", user.getEmail());
        } catch (Exception e){
            Log.d("user", e.getMessage());
            if (user == null){
                startLoginRegisterActivity();
                finish();
            }
        }

    }

    private void startLoginRegisterActivity(){
        startActivity(new Intent(MainActivity.this, LoginRegisterActivity.class));
    }
}
